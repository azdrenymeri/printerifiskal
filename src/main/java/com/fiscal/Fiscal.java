package com.fiscal;

import com.fiscal.data.ProductRepository;
import com.fiscal.ui.PrinteriFiskal;
import com.fiscal.utils.HideToSystemTray;
import com.fiscal.utils.Logger;
import com.fiscal.utils.PdfFileConverter;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Fiscal {
    private final static  String TAG = Fiscal.class.getSimpleName();
    private static boolean shouldRunThread = true;
    private  static List<String> messages;
    private static Thread thread;
    private  static PrinteriFiskal fiskal;
    public static PdfFileConverter pdfFileConverter;


    public static void main(String[] args)  {
            messages = new ArrayList<>();
            init();


    }

    public static JFrame  getFrame(){return fiskal;}

    public static void init(){
        Logger.d(TAG,"Printing Started");
        addMessage("Printimi Fiskal Startoi");
        Runnable r = new Runnable() {
            public void run() {

                fiskal = new PrinteriFiskal();
                new HideToSystemTray(fiskal);
                fiskal.setResizable(false);
                fiskal.show();
                try {
                    ProductRepository repository = new ProductRepository(fiskal);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                while (getState()){


                    fiskal.showMessage(messages);

                    try {
                       pdfFileConverter = new PdfFileConverter();
                        pdfFileConverter.checkForFile();
                        Thread.sleep(2000);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }



            }
        };

     thread = new Thread(r);
      thread.start();






    }



    public static void destroyThread(){
        fiskal.dispose();
        thread.stop();
    }
    public static void stopProcess(){
        stopLoop();
        stopThread();

    }
    public static void startProces(){
        startLoop();
        startThread();

    }
    public static boolean getState(){
        return shouldRunThread;
    }
    public static void stopLoop(){shouldRunThread = false;}
    public static void startLoop(){shouldRunThread = true;}
    static void stopThread(){thread.stop();}
    static void startThread(){thread.resume();}

    public static void addMessage(String message){
        messages.add(message);
    }

    public static void resetMessages(){
        messages.clear();
    }

}
