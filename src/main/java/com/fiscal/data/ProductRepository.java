package com.fiscal.data;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


        import com.fiscal.model.ProductRepItem;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 *
 * @author Azdren Ymeri
 */
public class ProductRepository {

    //writing classes
    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;
    private PrintWriter printWriter;

    //reading classes
    private FileReader fileReader;
    private BufferedReader bufferedReader;
    private String line = "";
    private File productsDir,productFile;
    private List<ProductRepItem> products;
    private File[] files;


    public ProductRepository(JFrame printeriFiskal) throws IOException{

        //creating dirs
        createDirs();

        //creating writing instances
        fileWriter = new FileWriter(productFile,true);
        bufferedWriter = new BufferedWriter(fileWriter);
        printWriter = new PrintWriter(bufferedWriter);
        //creating reading instances
        fileReader = new FileReader(productFile);
        bufferedReader = new BufferedReader(fileReader);

        if (readProductFile().length() <= 0){
            registerNewProduct("1 ; Test");
        }
        System.out.println( "PRODUCT: "+readProductFile());
        formatProducts(readProductFile());
    }

    public ProductRepository() throws IOException{
        //creating dirs
        createDirs();

        //creating writing instances
        fileWriter = new FileWriter(productFile,true);
        bufferedWriter = new BufferedWriter(fileWriter);
        printWriter = new PrintWriter(bufferedWriter);

        if (readProductFile().length() <= 0){
            registerNewProduct("1 ; Test");
        }
        System.out.println("Products: "+readProductFile());

        formatProducts(readProductFile());
    }

    private void createDirs(){
        //creating file
        productsDir = new File("C:\\products_of_fiscal_printer\\");
        if (!productsDir.exists()){ productsDir.mkdir();}

        productFile = new File(productsDir.getAbsoluteFile()+"\\products.txt");
        if (!productFile.exists()){
            try {
                productFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public String readProductFile() {
        StringBuffer buffer = new StringBuffer();
        //creating reading instances
        try {

        fileReader = new FileReader(productFile);
        bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line).append("\n");
            }



        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return buffer.toString();


    }

    public void formatProducts(String productsContent){
        List<String> unFormatedProducts = new ArrayList<>();
        List<String> sep = new ArrayList<>();
        products = new ArrayList<>();

        unFormatedProducts.addAll(Arrays.asList(productsContent.split("\n")));
        String con = null;
        for(int i =0;i<unFormatedProducts.size();i++){

            con = unFormatedProducts.get(i);
            System.out.println("Printing out con "+con);
            sep.addAll(Arrays.asList(con.split(";")));
            System.out.println("PLU: "+sep.get(0)+" Product Name: "+sep.get(1).trim());
            products.add(new ProductRepItem(Integer.parseInt(sep.get(0).trim()),sep.get(1)));
            sep.clear();
        }



    }
    public List<ProductRepItem> formatProductsAsList(String productsContent){
        List<String> unFormatedProducts = new ArrayList<>();
        List<String> sep = new ArrayList<>();
        List<ProductRepItem> products = new ArrayList<>();

        unFormatedProducts.addAll(Arrays.asList(productsContent.split("\n")));
        String con = null;
        for(int i =0;i<unFormatedProducts.size();i++){

            con = unFormatedProducts.get(i);
            System.out.println("Printing out con "+con);
            sep.addAll(Arrays.asList(con.split(" ; ")));
            System.out.println("Sep size is "+sep.size());
            System.out.println("sep.get(0):  "+sep.get(0));

           System.out.println("PLU: "+sep.get(0)+" Product Name: "+sep.get(1).trim());
           products.add(new ProductRepItem(Integer.parseInt(sep.get(0).trim()),sep.get(1)));
            sep.clear();
        }


        return products;
    }


    public  int getPLUByProductName(String productName){
        int PLU= -1;

        if(exists(productName)){
            PLU = getProductPlu(productName);

        }else if(!exists(productName)){
            registerNewProduct(productName,getPluInLine());
           PLU =  getProductPlu(productName);
        }

        return PLU;
    }
    private int getProductPlu(String productName){
        int plu = -1;
        for (int i = 0;i<products.size();i++){
            ProductRepItem prod = products.get(i);
            if (productName.equals(prod.getName().trim())){
                plu = prod.getPlu();

                break;
            }
        }
        return plu;
    }

    public void registerNewProduct(String produt){
        printWriter.println(produt);
        printWriter.close();

    }
    public void registerNewProduct(String product,int plu){
        String formatedProduct = plu+" ; "+product;
        printWriter.println(formatedProduct);
        printWriter.close();
    }
    public boolean exists(String product){

            product = product.trim();

        for (int i = 0;i<products.size();i++){
            ProductRepItem prod = products.get(i);
            if (product.equals(prod.getName().trim())){

                return true;

            }
        }

        return false;
    }

    public int getPluInLine(){
        int pluInLine = -1;
        List<ProductRepItem> prods = formatProductsAsList(readProductFile());
        pluInLine = prods.get(prods.size()-1).getPlu();
        pluInLine++;
        return pluInLine;
    }


}
