package com.fiscal.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JCheckboxEditor extends DefaultCellEditor {

        protected JCheckBox checkBox;
        private String label;
        private boolean isPushed;

        public JCheckboxEditor(JCheckBox checkBox) {
            super(checkBox);
            checkBox = new JCheckBox();
            checkBox.setOpaque(true);
            checkBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    fireEditingStopped();
                }
            });
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            if (isSelected) {
                checkBox.setForeground(table.getSelectionForeground());
                checkBox.setBackground(table.getSelectionBackground());
            } else {
                checkBox.setForeground(table.getForeground());
                checkBox.setBackground(table.getBackground());
            }
            label = (value == null) ? "" : value.toString();

            isPushed = true;
            return checkBox;
        }

        @Override
        public Object getCellEditorValue() {
            if (isPushed) {
                JOptionPane.showMessageDialog(checkBox, label + ": Ouch!");
            }
            isPushed = false;
            return label;
        }

        @Override
        public boolean stopCellEditing() {
            isPushed = false;
            return super.stopCellEditing();
        }
    }

