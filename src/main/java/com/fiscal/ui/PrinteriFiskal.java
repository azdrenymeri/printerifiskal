/*
 * Created by JFormDesigner on Fri Oct 13 00:44:44 CEST 2017
 */

package com.fiscal.ui;







import com.fiscal.Fiscal;
import com.fiscal.Paramentrat;
import com.fiscal.data.ProductRepository;
import com.fiscal.utils.CenterDialog;
import com.fiscal.utils.Logger;


import java.awt.*;
import java.awt.event.*;
import java.nio.file.Files;
import java.util.List;
import javax.swing.*;
import javax.swing.GroupLayout;
import java.io.*;

/**
 *
 * @author Azdren Ymeri
 */
public class PrinteriFiskal extends JFrame {
    private static final String TAG = PrinteriFiskal.class.getSimpleName();
    private DefaultListModel model;
    private int lastMessagesize = 0;
    private JPanel panel1;
    private ProductRepository repository;

    public PrinteriFiskal() {
        try {
            repository = new ProductRepository();
        } catch (IOException e) {
            e.printStackTrace();
        }
        initComponents();
    }

    private void btnParametersActionPerformed(ActionEvent e) {
        Paramentrat paramentrat =  new Paramentrat(Fiscal.getFrame());
        paramentrat.show();



    }



   public void showMessage(List<String> message){



        DefaultListModel<String> model = new DefaultListModel<>();
        for (int i =0;i<message.size();i++){
            model.add(i,message.get(i));
        }

        list1.setModel(model);

       int size = model.getSize();
       if (size > lastMessagesize ){
           list1.ensureIndexIsVisible(model.getSize()-1);
       }



        lastMessagesize = model.size();
    }

    private void btnRistartoActionPerformed(ActionEvent e) {
        // TODO add your code here
        Fiscal.resetMessages();




        Fiscal.destroyThread();
        Fiscal.addMessage("Ristartimi i programit");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        try {
            cleanPrinterFolder();
        } catch (IOException e1) {
            Logger.d(TAG,e1.getMessage());
        }


        Fiscal.init();


        Fiscal.addMessage("Programi u ristartua...");

    }
    private static void cleanPrinterFolder() throws IOException {
       File dirFile = new File("C:\\fiscal_printer");
       File[] files = dirFile.listFiles();
        if (files.length >= 0 ){
            System.out.println("FILES LENGTH IS BIGGER OR EQUAL WITH ZERO");
            for (int i =0;i<files.length;i++){
                File insideFile = new File(dirFile.getAbsoluteFile()+"\\"+files[i].getName());
                System.out.println("FILE PATH"+insideFile.getAbsoluteFile());
               Files.delete(insideFile.toPath());
            }

            System.out.println("Directory Cleared");
        }


    }

    private void btnLogActionPerformed(ActionEvent e) {
        Desktop desktop = Desktop.getDesktop();
        File file = new File("C:\\fiscal_printer_log");
        if (file.exists()){
            File[] files = file.listFiles();
            if (files.length >=0){
                File logFile = new File(file.getAbsoluteFile()+"\\"+files[0].getName());
                try {
                    desktop.open(logFile);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }


    private void btnStartStopActionPerformed(ActionEvent e) {

    }

    @Override
    public synchronized void addWindowListener(WindowListener l) {
        super.addWindowListener(l);
    }

    private void thisWindowClosing(WindowEvent e) {
        // TODO add your code here
        setExtendedState(JFrame.ICONIFIED);

    }

    private void menuItem3ActionPerformed(ActionEvent e) {
        // TODO add your code here

      int option =  JOptionPane.showConfirmDialog(Fiscal.getFrame(),"A jeni i sigurte qe do te ndaloni programin ?","Ndalo Programin",JOptionPane.OK_CANCEL_OPTION);
        if (option == 0){
            System.exit(0);
        }
   }

   private void menuItem7ActionPerformed(ActionEvent e) {
       // TODO add your code here

           Component component = (Component) e.getSource();
           JFrame frame = (JFrame) SwingUtilities.getRoot(component);

            AllProducts teGjithaProduktetDialog = new AllProducts(repository);
           teGjithaProduktetDialog.setTitle("Te Gjitha Produktet");
           new CenterDialog(teGjithaProduktetDialog);

   }

   private void menuItem8ActionPerformed(ActionEvent e)  {
       // TODO add your code here

           Component component = (Component) e.getSource();
           JFrame frame = (JFrame) SwingUtilities.getRoot(component);

           AddNewProduct shtoProdukt = new AddNewProduct(repository);
                    new CenterDialog(shtoProdukt,500,200);





   }

   private void menuItem4ActionPerformed(ActionEvent e) {
       // TODO add your code here
       File zFile = new File("src\\main\\java\\com\\fiscal\\data\\Zraport.inp");
       File movedFile = new File(zFile.getAbsolutePath());
        if (!movedFile.exists()){
            try {
                movedFile.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
       if (movedFile.exists()){
            movedFile.renameTo(new File("C:\\TEMP\\Zraport.inp"));
           JOptionPane.showMessageDialog(null,"Z raporti u printua","Z Raporti",JOptionPane.INFORMATION_MESSAGE);
       }
   }

   private void menuItem1ActionPerformed(ActionEvent e) {
       // TODO add your code here


   }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - mitter veski
        menuBar1 = new JMenuBar();
        menu1 = new JMenu();
        menuItem3 = new JMenuItem();
        menu5 = new JMenu();
        menuItem7 = new JMenuItem();
        menuItem8 = new JMenuItem();
        label1 = new JLabel();
        scrollPane1 = new JScrollPane();
        list1 = new JList();
        label2 = new JLabel();
        btnLog = new JButton();

        //======== this ========
        setIconImage(new ImageIcon("C:\\Users\\Azdren\\Desktop\\fp7009.jpg").getImage());
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                thisWindowClosing(e);
                thisWindowClosing(e);
            }
        });
        Container contentPane = getContentPane();

        //======== menuBar1 ========
        {

            //======== menu1 ========
            {
                menu1.setText("File");

                //---- menuItem3 ----
                menuItem3.setText("Ndalo Programin");
                menuItem3.setForeground(Color.red);
                menuItem3.addActionListener(e -> menuItem3ActionPerformed(e));
                menu1.add(menuItem3);
            }
            menuBar1.add(menu1);

            //======== menu5 ========
            {
                menu5.setText("Produktet");

                //---- menuItem7 ----
                menuItem7.setText("Te Gjitha Produktet");
                menuItem7.addActionListener(e -> menuItem7ActionPerformed(e));
                menu5.add(menuItem7);

                //---- menuItem8 ----
                menuItem8.setText("Shto Produkt Te Ri");
                menuItem8.addActionListener(e -> menuItem8ActionPerformed(e));
                menu5.add(menuItem8);
            }
            menuBar1.add(menu5);
        }
        setJMenuBar(menuBar1);

        //---- label1 ----
        label1.setText("Printeri Fiskal");
        label1.setFont(new Font("Segoe UI", Font.BOLD, 22));
        label1.setForeground(Color.blue);

        //======== scrollPane1 ========
        {

            //---- list1 ----
            list1.setForeground(Color.green);
            list1.setBackground(Color.black);
            scrollPane1.setViewportView(list1);
        }

        //---- label2 ----
        label2.setText("Veprimet e fundit");

        //---- btnLog ----
        btnLog.setText("Log");
        btnLog.setFont(new Font("Segoe UI", Font.BOLD, 20));
        btnLog.addActionListener(e -> btnLogActionPerformed(e));

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(label2, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(label1, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                            .addComponent(btnLog, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(btnLog, GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                            .addComponent(label1))
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addGap(0, 10, Short.MAX_VALUE)
                            .addComponent(label2, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 317, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }



    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - mitter veski
    private JMenuBar menuBar1;
    private JMenu menu1;
    private JMenuItem menuItem3;
    private JMenu menu5;
    private JMenuItem menuItem7;
    private JMenuItem menuItem8;
    private JLabel label1;
    private JScrollPane scrollPane1;
    private JList list1;
    private JLabel label2;
    private JButton btnLog;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
