package com.fiscal.adapter;

import com.fiscal.model.Product;
import com.fiscal.model.ProductRepItem;

import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;

public class TableModels extends AbstractTableModel {

    private List<ProductRepItem> products;
    public TableModels(List<ProductRepItem> products){
        this.products = products;
    }

    @Override
    public int getRowCount() {
        return products.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        List<ProductRepItem> productRepItem = Arrays.asList(products.get(rowIndex));
        return productRepItem.get(columnIndex);

    }
}
