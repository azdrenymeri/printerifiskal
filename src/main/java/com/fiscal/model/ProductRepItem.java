package com.fiscal.model;
/**
 *
 * @author Azdren
 */
public class ProductRepItem {
    private int plu;
    private String name;

    public ProductRepItem(int plu,String name){
        this.plu = plu;
        this.name = name;

    }

    public int getPlu() {
        return plu;
    }

    public void setPlu(int plu) {
        this.plu = plu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return plu+" ; "+name;
    }



}