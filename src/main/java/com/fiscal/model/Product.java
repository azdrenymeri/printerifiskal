package com.fiscal.model;

import java.security.PublicKey;

public class Product {
    private String description;
    private String quantity;
    private String price;

    public Product(String description, String quantity, String price) {
        this.description = description;
        this.quantity = quantity;
        this.price = price;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return description +" "+quantity+" "+price;
    }
}
