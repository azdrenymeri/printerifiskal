package com.fiscal.utils;




import com.fiscal.Fiscal;
import com.fiscal.data.ProductRepository;
import com.fiscal.model.Product;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
/**
 *
 * @author Azdren Ymeri
 */
public class PdfFileConverter {
    private static final String TAG = PdfFileConverter.class.getSimpleName();
    private  File dirFile, pdfFile, parsedFile;
    private  File files[];
    private String receiptData;
    private PdfReader reader;
    String euroSign = Character.toString('\u20ac');
    String eSign = Character.toString('\u00EB');

    public PdfFileConverter() throws Exception {

        //todo AY : Make possible to extract data even if the product name has two lines  of charcters


    }




    public void checkForFile(){
        File dir = new File("C:\\fiscal_printer");
        if (!dir.exists()){ dir.mkdir();}
        File[] files = dir.listFiles();
        if (files != null){
            if (files.length > 0){

                File file = new File(dir.getAbsoluteFile()+"\\"+files[0].getName());
                String extension = getFileExtension(file);

              if (extension.equalsIgnoreCase("pdf")){
                  if (file.length() > 0){

                          parsePdf();

                  }else{

                      Fiscal.addMessage("Dokumenti "+files[0].getName()+" eshte i deformuar ju lutem provoni perseri");
                      try {
                          Files.delete(file.toPath());
                      } catch (IOException e) {
                          e.printStackTrace();
                      }
                  }
              }else {

                  Fiscal.addMessage("Formati "+extension+" i dokumentit \""+files[0].getName()+"\" nuk perkrahet nga printeri");
                  try {
                      Files.delete(file.toPath());
                  } catch (IOException e) {
                      e.printStackTrace();
                      Logger.d(TAG,String.valueOf(e.getStackTrace()));
                  }
              }
            }else{

            }

        }

    }



    public void parsePdf()  {
        Fiscal.addMessage("----- FILLIMI -----");

        dirFile = new File("C:\\fiscal_printer");
        if (!dirFile.exists()) {
            dirFile.mkdir();
        }
        files = dirFile.listFiles();
        if (files != null && files.length >= 0) {
            pdfFile = new File("C:\\fiscal_printer\\" + files[0].getName());
            System.out.println("There are " + files.length + " in the fiscal_printer dir");
        }

        try {

            reader = new PdfReader("C:\\fiscal_printer\\" + files[0].getName());

                for (int pages = 1 ;pages <= reader.getNumberOfPages();pages++){
                    // pageNumber = 1
                    receiptData += PdfTextExtractor.getTextFromPage(reader, pages);
                }


                System.out.println("TEXT FROM PAGE 1: " + receiptData);

                reader.close();





            Files.delete(pdfFile.toPath());



            Fiscal.addMessage("Duke Procesuar...");

            System.out.println("================================================");
            System.out.println("               Full Receipt                     ");
            System.out.println("================================================");


            if (receiptData != null) {
                System.out.println(receiptData);

                if (receiptData.contains("Description")){
                    formatReceipt(receiptData);
                }else if(receiptData.contains("P"+eSign+"rshkrimi"))
                secondFormatReceipt(receiptData);

            }


        }catch (IOException e) {
            e.printStackTrace();
            Logger.d(TAG,String.valueOf(e.getStackTrace()));
        }




    }



    public void formatReceipt(String receipt){


        System.out.println("================================================");
        System.out.println("             Formatting Receipt FAZE 1          ");
        System.out.println("================================================");

        Fiscal.addMessage("Formatimi i dokumentit");


        List<String> array = new ArrayList<>(Arrays.asList(receipt.split("\\n")));

        for (int i =0; i<array.size();i++){

            System.out.println("element "+" : "+i+" "+array.get(i));

        }
        System.out.println("================================================");
        System.out.println("             Cleaning Header/Footer             ");
        System.out.println("================================================");

        Fiscal.addMessage("Validimi dokumentit");

        System.out.println("\n\n\n\n\n\n\n\n  Filtering Data \n\n\n\n\n");
        int index = 0;

        Iterator<String> it = array.iterator();
        while (it.hasNext()){

            if (it.next().contains("Description")){

                break;
            }
            it.remove();

        }





        for (int size = array.size()-1;size < array.size();size--){
            if (array.get(size).contains("Taxes")){
                array.remove(size);
                break;
            }
            array.remove(size);
        }

        System.out.println("================================================");
        System.out.println("             Only Orders                        ");
        System.out.println("================================================");


        array.remove(0);
        for (int i =0; i<array.size();i++){
            System.out.println("element "+" : "+i+" "+array.get(i));
        }


        String s = "";
        String s2 = "";
        List<String> word = new ArrayList<>();
        List<String> number = new ArrayList<>();
        List<String> sepNumber = new ArrayList<>();
        List<Product> productList = new ArrayList<>();
        String sepString = "";


        for (int iterator = 0; iterator < array.size();iterator++){

            s = array.get(iterator);

            s2 = array.get(iterator);

            word.add(s.replaceAll("\\d+[.]\\d+"," "));

            number.add(s2.replaceAll("[^0-9\\\\.]+"," "));

            sepString = number.get(0);

            System.out.println("Part Two Trimed: "+sepString);

            sepNumber.addAll( Arrays.asList(sepString.split(" +")));


            System.out.println(" ARR:   "+sepNumber.get(0)+" : " + sepNumber.get(1)+" "+sepNumber.get(2));


            productList.add(new Product(
                    ""+word.get(0)
                            .replace(euroSign,"")
                            .replace("Description","")
                            .replace("Quantity"," ")
                            .replace("Price"," ")
                            .trim()+"",
                            ""+sepNumber.get(1)+"",
                            ""+sepNumber.get(2)+""));


             word.clear();
             number.clear();


        }




        for (int v = 0; v<productList.size();v++){
            Product product = productList.get(v);
            System.out.println("ID: "+v+"\nProduct: "+ product.getDescription()+"\n Quantity: "+product.getQuantity()+"\nPrice: "+product.getPrice());
            Fiscal.addMessage("Produkti: "+ product.getDescription()+" Sasia: "+product.getQuantity()+" Qmimi: "+product.getPrice()+" "+euroSign);

        }




        addProductsToTheFiscalPrinter(productList);



    }

    public void secondFormatReceipt(String receipt){


        System.out.println("================================================");
        System.out.println("             Formatting Receipt FAZE 1          ");
        System.out.println("================================================");

        Fiscal.addMessage("Formatimi i dokumentit");


        List<String> array = new ArrayList<>(Arrays.asList(receipt.split("\\n")));

        for (int i =0; i<array.size();i++){

            System.out.println("element "+" : "+i+" "+array.get(i));

        }
        System.out.println("================================================");
        System.out.println("             Cleaning Header/Footer             ");
        System.out.println("================================================");

        Fiscal.addMessage("Validimi dokumentit");

        System.out.println("\n\n\n\n\n\n\n\n  Filtering Data \n\n\n\n\n");
        int index = 0;

        Iterator<String> it = array.iterator();
        while (it.hasNext()){

            if (it.next().contains("P"+eSign+"rshkrimi")){

                break;
            }
            it.remove();

        }





        for (int size = array.size()-1;size < array.size();size--){
            if (array.get(size).contains("Taxes")){
                array.remove(size);
                break;
            }
            array.remove(size);
        }

        System.out.println("================================================");
        System.out.println("             Only Orders                        ");
        System.out.println("================================================");


        array.remove(0);
        for (int i =0; i<array.size();i++){
            System.out.println("element "+" : "+i+" "+array.get(i));
        }


        String s = "";
        String s2 = "";
        List<String> word = new ArrayList<>();
        List<String> number = new ArrayList<>();
        List<String> sepNumber = new ArrayList<>();
        List<Product> productList = new ArrayList<>();
        String sepString = "";


        for (int iterator = 0; iterator < array.size();iterator++){

            s = array.get(iterator);

            s2 = array.get(iterator);

            word.add(s.replaceAll("\\d+[,]\\d+"," "));

            number.add(s2.replaceAll("[^0-9\\\\,]+"," "));

            sepString = number.get(0);

            System.out.println("Part Two Trimed: "+sepString);

            sepNumber.addAll( Arrays.asList(sepString.split(" +")));


            System.out.println(" ARR:   "+sepNumber.get(0)+" : " + sepNumber.get(1)+" "+sepNumber.get(2));


            productList.add(new Product(
                    ""+word.get(0)
                            .replace(euroSign,"")
                            .replace("P"+eSign+"rshkrimi","")
                            .replace("Quantity"," ")
                            .replace("Price"," ")
                            .trim()+"",
                    ""+sepNumber.get(1)+"",
                    ""+sepNumber.get(2)+""));


            word.clear();
            number.clear();


        }




        for (int v = 0; v<productList.size();v++){
            Product product = productList.get(v);
            System.out.println("ID: "+v+"\nProduct: "+ product.getDescription()+"\n Quantity: "+product.getQuantity()+"\nPrice: "+product.getPrice());
            Fiscal.addMessage("Produkti: "+ product.getDescription()+" Sasia: "+product.getQuantity()+" Qmimi: "+product.getPrice()+" "+euroSign);

        }




        addProductsToTheFiscalPrinter(productList);



    }

    public static void addProductsToTheFiscalPrinter(List<Product> productList){

            File secondDirFile = new File("C:\\printer_receipt\\");
            if (!secondDirFile.exists()){secondDirFile.mkdir();}

            File saleFile = new File("C:\\printer_receipt\\product_sale.inp");
            if (!saleFile.exists()){
                try {
                    saleFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        Writer write = null;
        try {
            write = new FileWriter(saleFile,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(write);
            PrintWriter printWriter = new PrintWriter(bufferedWriter);

            String saleContent = "";
            String quanti = "";


        ProductRepository repo = null;
        try {
            repo = new ProductRepository();
            repo.formatProducts(repo.readProductFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        repo.readProductFile();
        for(int g = 0;g< productList.size();g++){
                Product product =  productList.get(g);


                saleContent = "S,1,______,_,__;"+product.getDescription()+";"+product.getPrice()+";"+product.getQuantity()+";1;1;5;0;"+repo.getPLUByProductName(product.getDescription().trim()) +";";
                printWriter.println(saleContent);
            }

            printWriter.print("T,1,______,_,__;");
            printWriter.close();

            File tempFile = new File("C:\\TEMP\\");
            if (!tempFile.exists()){tempFile.mkdir();}


            File inFile = new File("C:\\TEMP\\sell_product.inp");
            if (inFile.exists()){
                try {
                    Files.delete(inFile.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            saleFile.renameTo(inFile) ;


        Fiscal.addMessage("----- FUNDI -----");

    }



    String getFileExtension(File file){
        String extension = "";
        String fileName = file.getAbsolutePath();
        int i = fileName.lastIndexOf('.');
        int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));

        if (i > p) {
            extension = fileName.substring(i+1);
        }

        return extension;
    }

}
