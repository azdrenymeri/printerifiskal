package com.fiscal.utils;

import jdk.nashorn.internal.scripts.JD;

import javax.swing.*;
import java.awt.*;

public class CenterDialog {

    private Image image;

    public CenterDialog(JDialog d){
      image=Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/img/fp.png"));
        d.setSize(300, 300);
        d.setIconImage(image);
        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        final Dimension screenSize = toolkit.getScreenSize();
        final int x = (screenSize.width - d.getWidth()) / 2;
        final int y = (screenSize.height - d.getHeight()) / 2;
        d.setLocation(x, y);
        d.setVisible(true);
    }
    public CenterDialog(JDialog d,int width,int heitht){
        image=Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/img/fp.png"));
        d.setSize(width, heitht);
        d.setIconImage(image);
        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        final Dimension screenSize = toolkit.getScreenSize();
        final int x = (screenSize.width - d.getWidth()) / 2;
        final int y = (screenSize.height - d.getHeight()) / 2;
        d.setLocation(x, y);
        d.setVisible(true);
    }
}
