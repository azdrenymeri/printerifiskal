package com.fiscal.utils;

import sun.rmi.runtime.Log;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    private static OutputStream outputStream;
    private static OutputStreamWriter writer;
    private static File fileDir;
    private static File fileLog;
    private static File[] files;
    private static FileWriter fileWriter;
    private static BufferedWriter bw;
    private static PrintWriter out;

    public static void d(String Tag, String message) {
        try {
        DateFormat dateForma = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String dateFormat =dateForma.format(date);
        fileDir = new File("C:\\fiscal_printer_log");
        if(fileDir.exists()){
            files = fileDir.listFiles();
            fileLog = new File("C:\\fiscal_printer_log\\" + files[0].getName());

            String mainString = "LOG: " + dateFormat + ":::\n Class: " + Tag + "\n Message: " + message;


            fileWriter = new FileWriter(fileLog, true);
            bw = new BufferedWriter(fileWriter);
            out = new PrintWriter(bw);

            out.println(mainString);

            out.close();
        }else if(!fileDir.exists()){
            fileDir.mkdir();
            fileLog = new File(fileDir.getAbsoluteFile()+"\\"+"log.txt");
            if (!fileLog.exists()){
                fileLog.createNewFile();
            }

            String mainString = "LOG: " + dateFormat + ":::\n Class: " + Tag + "\n Message: " + message;


            fileWriter = new FileWriter(fileLog, true);
            bw = new BufferedWriter(fileWriter);
            out = new PrintWriter(bw);


            out.println(mainString);

            out.close();
        }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}